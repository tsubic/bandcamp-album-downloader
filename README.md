#Bandcamp album downloader

run
'''python3 band.py URL ALBUM_TITLE ARTIST
'''

where ALBUM_TITLE and ARTIST may be optional. Try it without them, the program will try to get the data by itself, if it fails, you have to specify it manually. URL is the url of the bandcamp album you want to download.