#!/usr/bin/python3

import requests
import re
from bs4 import BeautifulSoup
import json
import wget
import os,sys
from PIL import Image
import taglib

class Song():
    def __init__(self, number, title, artist, album, url, img):
        self.number = number
        self.title = title
        self.artist = artist
        self.album = album
        self.url = url
        self.img = img

url = sys.argv[1] #'http://mortbend.bandcamp.com/album/odjel-za-e-e'

#get and parse info about songs and album

r = requests.get(url)

soup = BeautifulSoup(r.text)

data = soup.find_all("script")

album_title = "Unknown"
artist = "Unknown"


for line in data:
    
    iurl = re.findall('artFullsizeUrl:\s".*"', str(line))
    if iurl:
        image_url = iurl[0][17:-1]
        print(image_url)
    tf = re.findall('trackinfo\s:\s\[.*\]', str(line))
    if tf:
        trackinfo = tf[0][12:]
        tracks = json.loads(trackinfo)
        print (tracks[0]['track_num'])
    pkg = re.findall('packages:\s\[.*\]', str(line))
    
    try:
        packages = pkg[0][10:]
        p = json.loads(packages)
#        print (p[0]['download_title'])
        try:
            album_title =  p[0]['download_title']
        except:
            album_title = p[0]['album_title']
        artist = p[0]['download_artist']
    except:
        if(len(sys.argv) >2):
            album_title = sys.argv[2]
            artist = sys.argv[3]
        else:
            print("Please put in album title and name manually after url.\npython3 url 'albumtitle' 'band name'")
            sys.exit()
songs = []

os.mkdir(album_title+ " " +artist)
os.chdir(album_title+ " " +artist)



img = wget.download(image_url, 'cover_big.jpg')

#resize album cover image to 300,300

size = 300,300

try:
    im = Image.open(img)
    im.thumbnail(size, Image.ANTIALIAS)
    im.save("cover.jpg", "jpeg")
    print(im)
except IOError:
    print ("cannot create cover image")

#create songs object list

for track in tracks:
    if track['track_num'] in range(9):
        number  = "0" + str(track['track_num'])
    else:
        number = str(track['track_num'])
    title = track['title']
    url = track['file']['mp3-128']

    songs.append(Song(number, title, artist, album_title, url, 'cover.jpg'))

#start the job
for s in songs:
    print (s.number, s.title, s.artist, s.url)
    filename = s.number+" "+s.title+".mp3"
    surl = "http:" + s.url
    wget.download(surl, filename)

    f = taglib.File(filename)
    f.tags["ALBUM"] = s.album
    f.tags["ARTIST"] = s.artist
    f.tags["TRACKNUMBER"] = s.number
    f.tags["TITLE"] = s.title
    retval = f.save()
    print(retval)
